import React from 'react';
import {connect} from 'react-redux'
import { buyCake } from '../redux';

function CakeContainer(props) {
    return (
        <div>
            <h2>Number of cake - {props.numOfCakes}</h2>
            <button onClick={props.buyCake}>buy a cake</button>
        </div>
    );
}

const mapStateToProps = state => {
    return{
        numOfCakes : state.cake.numOfCake
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyCake: () => dispatch(buyCake())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (CakeContainer) 